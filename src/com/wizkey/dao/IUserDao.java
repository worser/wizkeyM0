package com.wizkey.dao;

import com.wizkey.pojo.User;

/**
 * User类Dao接口
 * Description:当基本接口方法不再满足需求时可在子接口中定义
 */
public interface IUserDao extends IBaseDao<User> {
}