package com.wizkey.dao;

import com.wizkey.pojo.Comment;

public interface ICommentDao extends IBaseDao<Comment>{
	public boolean addComment(Comment comment, String userId, String assetId);

	boolean addScore(Comment comment, String userId, String assetId);
}
