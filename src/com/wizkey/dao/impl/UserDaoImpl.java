package com.wizkey.dao.impl;

import org.springframework.stereotype.Repository;

import com.wizkey.dao.IUserDao;
import com.wizkey.pojo.User;

/**
 * 用户接口实现类
 * <p>
 * ClassName: UserDaoImpl
 * </p>
 */
@Repository
public class UserDaoImpl extends BaseDaoImpl<User> implements IUserDao {
	
	@Override
	protected Class<User> getEntityClass() {
		return User.class;
	}

}