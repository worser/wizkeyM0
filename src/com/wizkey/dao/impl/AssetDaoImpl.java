package com.wizkey.dao.impl;

import org.springframework.stereotype.Repository;

import com.wizkey.dao.IAssetDao;
import com.wizkey.pojo.Asset;

/**
 * 方案接口实现类
 * <p>
 * ClassName: AssetDaoImpl
 * </p>
 */
@Repository
public class AssetDaoImpl extends BaseDaoImpl<Asset> implements IAssetDao{

	@Override
	protected Class<Asset> getEntityClass() {
		return Asset.class;
	}

}
