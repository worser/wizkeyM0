package com.wizkey.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wizkey.pojo.Comment;
import com.wizkey.pojo.User;
import com.wizkey.service.ICommentService;
import com.wizkey.service.IUserService;

@Controller
@RequestMapping("/test")
public class TestController {
	@Autowired
	private IUserService service;
	@Autowired
	private ICommentService commentservice;
	
	//类，返回给客户端json
	@RequestMapping(value = "/regist", method = RequestMethod.GET)
	@ResponseBody 
	public Comment regist() {
		Comment comment=commentservice.find("5a3cb193e746e2e5a78bf23c");
		return comment;
	}
	
	//客户端body传json，服务端接收到类
	@RequestMapping(value = "/regist2", method = RequestMethod.POST)
	@ResponseBody 
	public String regist(@RequestBody User user) {
		System.out.println("客户端请求的类"+user);
		System.out.println("userId:"+user.getId());
		return "sucess";
	}
	
	// 客户端head传k-v，服务端接收参数
	@RequestMapping(value = "/regist4", method = RequestMethod.POST)
	@ResponseBody
	public String regist(@RequestHeader("username") String username, @RequestHeader("password") String password) {
		System.out.println("name=" + username + ",pw=" + password);
		return "sucess";
	}
	
	// 客户端body传k-v（或者表单），服务端接收到参数 【username=fanfff&password=123 {raw}】【username:f  	password:ff{x-www-form-urlencoded}】
	@RequestMapping(value = "/regist5", method = RequestMethod.POST)
	@ResponseBody
	public String regist5(@RequestParam("username") String username, @RequestParam("password") String password) {
		System.out.println("name=" + username + ",pw=" + password);
		return "sucess";
	}
	
//	// 客户端body传k-v，服务端接收到类【"id":"5a3c9c48e7467037923c3455","name":"ws","age":5,"birth":1513921607395,"password":"123","commentsId":[]】
//	@RequestMapping(value = "/regist6", method = RequestMethod.POST)
//	@ResponseBody
//	public String regist6(@ModelAttribute User user) {
//		System.out.println("客户端请求的类"+user);
//		return "sucess";
//	}
	
	

}
