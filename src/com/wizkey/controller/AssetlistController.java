package com.wizkey.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wizkey.service.IAssetService;

@Controller
@RequestMapping("/list")
public class AssetlistController {
	
	@Autowired
	private IAssetService service;
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	@ResponseBody
	public Object search(@RequestParam("keyword") String keyword, @RequestParam("pageNo") String pageNo) {
		return service.getAssetInList(keyword, pageNo);
	}
	
	@RequestMapping(value = "/hotspots", method = RequestMethod.GET)
	@ResponseBody
	public Object hotspots(@RequestParam("pageNo") String pageNo) {
		return service.getMoreAsset("commentNum", pageNo);
	}
	
	@RequestMapping(value = "/latests", method = RequestMethod.GET)
	@ResponseBody
	public Object latests(@RequestParam("pageNo") String pageNo) {
		return service.getMoreAsset("modifiedDate", pageNo);
	}
	
}
