package com.wizkey.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wizkey.pojo.Asset;
import com.wizkey.service.IAssetService;

@Controller
@RequestMapping("/index")
public class AssetController {
	
	@Autowired
	private IAssetService service;
	
	//首页请求热门方案
	@RequestMapping(value = "/hotspots", method = RequestMethod.GET)
	@ResponseBody
	public Object indexHotspots(){
		List<Asset> list = service.getAssetInIndex("commentNum");
		return list;
	}
	
	//首页请求最新方案
	@RequestMapping(value = "/latests", method = RequestMethod.GET)
	@ResponseBody
	public Object indexLatests(){
		List<Asset> list = service.getAssetInIndex("modifiedDate");
		return list;
	}
}
