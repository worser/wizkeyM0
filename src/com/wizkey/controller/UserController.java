package com.wizkey.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wizkey.pojo.Comment;
import com.wizkey.pojo.User;
import com.wizkey.service.IUserService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private IUserService service;

	/**
	 * regist注册
	 * @return
	 */
	
	@RequestMapping(value = "/regist", method = RequestMethod.GET)
	@ResponseBody
	public String regist() {
		/**
		 * GET请求返回null,用户注册页面
		 */
		return null;
	}

	@RequestMapping(value = "/regist", method = RequestMethod.POST)
	@ResponseBody
	public Object regist(@RequestParam("username") String name,@RequestParam("password") String password,
			@RequestParam("age") String age,
			HttpServletResponse response, HttpServletRequest request,
			HttpSession session)
			throws IOException {
		User user = new User(name, Integer.parseInt(age), new Timestamp(System.currentTimeMillis()), password);
		if (service.existUserName(name)) {
			request.setAttribute("errorMsg", "该用户已存在！");
			/**
			 * 注册时用户名已存在返回1
			 */
			return "1";
		}
		service.save(user);
		/**
		 * 注册成功返回注册用户名
		 */
		return user.getName();

	}

	/**
	 * login登录
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	@ResponseBody
	public String login() {
		/**
		 * GET请求返回null,用户登录页面
		 */
		return null;
	}

	@ResponseBody
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public Object login(@RequestParam("username") String username, @RequestParam("password") String password,
			Model model, HttpSession session,
			HttpServletResponse response, HttpServletRequest request) {

		User user = service.getUserByPassword(username, password);
		if(user==null){
			request.setAttribute("errorMsg", "用户名或密码错误！");
			/**
			 * 登录时用户名或密码错误返回1
			 */
			return "1";
			}
		session.setAttribute("username", username);
		Cookie cookie=new Cookie("JSESSIONID",session.getId());
		cookie.setMaxAge(60*60*60*24);
		response.addCookie(cookie);
		/**
		 * 登录成功返回用户名
		 * 在session中添加username
		 * 将cookie持久化为1天
		 */
		return user.getName();
	}
	
	/**
	 * logout退出
	 * @return
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	@ResponseBody
	public String logout() {
		/**
		 * GET请求返回null,用户退出页面
		 */
		return null;
	}
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	@ResponseBody
	public String logout(HttpSession session,
			HttpServletResponse response, HttpServletRequest request) {
		if(session.getAttribute("username")!=null){
			session.invalidate();
			Cookie cookie=new Cookie("JSESSIONID",session.getId());
			cookie.setMaxAge(0);
			cookie.setPath("");
			response.addCookie(cookie);
			}
		/**
		 * 退出成功返回0,跳转至login页面
		 */
		return "0";
	}
	
	
	/**
	 * myenvents我的活动：此处为我的评论
	 * TODO 分页查找
	 */
	@RequestMapping(value = "/myenvents/mycomment", method = RequestMethod.GET)
	@ResponseBody
	public Object myenvents(HttpSession session) {
		String username = (String)session.getAttribute("username");
		if(username==null){
			/**
			 * 查看我的评论需要登录,返回1
			 */
			return "1";
		}
		User user = service.getUserByName(username);
		List<Comment> comments=service.getCommentsById(user.getId());
		/**
		 * 可以查看我的评论返回评论列表
		 */
		return comments;
	}
}