package com.wizkey.controller;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wizkey.pojo.Comment;
import com.wizkey.service.ICommentService;

@Controller
@RequestMapping("/index")
public class CommentController {
	@Autowired
	private ICommentService commentService;

	/**
	 * body:Comment{string comment,int score}
	 * head:assetId
	 * 注：comment可为null,无score值设为0
	 * return: true false "illegal"
	 */
	@RequestMapping(value = "/comment", method = RequestMethod.POST)
	@ResponseBody
	public Object AddComment(@RequestBody Comment comment, @RequestHeader("assetId") String assetId,
			HttpServletResponse response, HttpSession session, Model model) throws IOException {
		System.out.println(comment+assetId);
		return commentService.addComment(comment, session.getId(), assetId);
	}
}
