package com.wizkey.pojo;

import java.sql.Timestamp;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Comment {
	@Id
	private String id;
	
	private String comment;
	
	private Timestamp commentTime;
	
	private int score;
	/**
	 * 评论状态 state=0，默认可见状态；state=1，审核不可见；state=2，删除不可见
	 */
	private int state = 0;
	
	private String userID;
	
	private String assetID;
	
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Timestamp getCommentTime() {
		return commentTime;
	}
	public void setCommentTime(Timestamp commentTime) {
		this.commentTime = commentTime;
	}
	
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	
	public String getAssetID() {
		return assetID;
	}
	public void setAssetID(String assetID) {
		this.assetID = assetID;
	}
	
	public Comment(String comment, int score) {
		super();
		this.comment = comment;
		this.score = score;
	}
	public Comment(String comment, Timestamp commentTime, int score, int state) {
		super();
		this.comment = comment;
		this.commentTime = commentTime;
		this.score = score;
		this.state = state;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public Comment() {
		super();
	}
	@Override
	public String toString() {
		return "Comment [comment=" + comment + ", commentTime=" + commentTime + ", score=" + score + "]";
	}
	
	
}
