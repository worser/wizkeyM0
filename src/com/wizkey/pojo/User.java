package com.wizkey.pojo;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * 用户实体类
 */
@Document(collection = "user")
public class User {

	@Id
	private String id;
	@Field
	@Indexed(unique = true) 
	private String name;
	
	private int age;
	
	private Timestamp birth;
	
	private String password;
	@DBRef
	private List<Comment> comments = new ArrayList<Comment>();
	//private Set<String> commentsId = new HashSet<String>();
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Timestamp getBirth() {
		return birth;
	}

	public void setBirth(Timestamp birth) {
		this.birth = birth;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

//	public Set<String> getCommentsId() {
//		return commentsId;
//	}
//
//	public void setCommentsId(Set<String> commentsId) {
//		this.commentsId = commentsId;
//	}

	public User() {
		super();
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public User(String name, int age, Timestamp birth, String password) {
		super();
		this.name = name;
		this.age = age;
		this.birth = birth;
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [ name=" + name + ", age=" + age + ", birth=" + birth + ", password=" + password
				+ "]";
	}
	
}