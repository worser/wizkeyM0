package com.wizkey.pojo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class AssetComment{
	@Id
	private String assetId;
	
	private List<String> commentIds = new ArrayList<String>();
	
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public List<String> getCommentIds() {
		return commentIds;
	}
	public void setCommentIds(List<String> commentIds) {
		this.commentIds = commentIds;
	}
	@Override
	public String toString() {
		return "AssetComment [assetId=" + assetId + ", commentIds=" + commentIds + "]";
	}

}
