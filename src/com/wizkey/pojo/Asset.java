package com.wizkey.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class Asset {
	/**
	 * 方案ID
	 */
	@Id
	private String id;
	/**
	 * 方案url
	 */
	private String url;
	/**
	 * 方案标题
	 */
	private String title;
	/**
	 * 软件版本号
	 */
	private String version;
	/**
	 * 修改日期
	 */
	private Date modifiedDate;
	/**
	 * 用户评分
	 */
	private float score;
	/**
	 * 用户评分人数
	 */
	private int scoreNum;
	/**
	 * 用户评论量
	 */
	private int commentNum;
	/**
	 * 评论id list
	 */
	private List<String> commentIds = new ArrayList<String>();

	public int getScoreNum() {
		return scoreNum;
	}

	public void setScoreNum(int scoreNum) {
		this.scoreNum = scoreNum;
	}

	public List<String> getCommentIds() {
		return commentIds;
	}

	public void setCommentIds(List<String> commentIds) {
		this.commentIds = commentIds;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public int getCommentNum() {
		return commentNum;
	}

	public void setCommentNum(int commentNum) {
		this.commentNum = commentNum;
	}

	public Asset(String url, String title, String version, Date modifiedDate) {
		super();
		this.url = url;
		this.title = title;
		this.version = version;
		this.modifiedDate = modifiedDate;
		this.score = 0.0f;
		this.commentNum = 0;
	}

	public Asset(String url, String title, String version, Date modifiedDate, float score, int commentNum) {
		super();
		this.url = url;
		this.title = title;
		this.version = version;
		this.modifiedDate = modifiedDate;
		this.score = score;
		this.commentNum = commentNum;
	}

	public Asset() {
		super();
	}

	@Override
	public String toString() {
		return "Asset [id=" + id + ", url=" + url + ", title=" + title + ", version=" + version + ", modifiedDate="
				+ new SimpleDateFormat("yyyy-MM-dd").format(modifiedDate) + ", score=" + score + ", commentNum=" + commentNum + "]";
	}
	
}
