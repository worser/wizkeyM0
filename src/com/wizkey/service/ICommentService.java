package com.wizkey.service;

import com.wizkey.pojo.Comment;

public interface ICommentService extends IBaseService<Comment>{
	/**
	 * 添加一条评论,同时为Asset类更新score值
	 * <p>
	 * ClassName: addComment
	 * </p>
	 */
	public Object addComment(Comment comment,String userId,String assetId);
	
	
}
