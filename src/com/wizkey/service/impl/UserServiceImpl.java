package com.wizkey.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wizkey.dao.IBaseDao;
import com.wizkey.dao.ICommentDao;
import com.wizkey.dao.IUserDao;
import com.wizkey.pojo.Comment;
import com.wizkey.pojo.User;
import com.wizkey.service.IUserService;

/**
 * 用户service类
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements IUserService{
	@Autowired
	protected IUserDao userDao;
	@Autowired
	protected ICommentDao commentDao;
 
	@Override
	protected IBaseDao<User> getDao() {
		return userDao;
	}

	@Override
	public User getUserByPassword(String name,String password) {
		User existU=userDao.findOneByProp( "name" ,  name );
		if(existU==null){
			return null;
		}
		if(!existU.getPassword().equals(password)){
			return null;
		}
		return existU;
	}

	@Override
	public boolean existUserName(String name) {
		User existU=userDao.findOneByProp("name", name);
		if(existU==null){			
			return false;
		}
		return true;
	}

	@Override
	public User getUserByName(String username) {
		User existU=userDao.findOneByProp("name", username);
		if(existU==null){			
			return null;
		}
		return existU;
	}

	@Override
	public List<Comment> getCommentsById(String id) {
		//TODO 分页查找
		return commentDao.findByProp("userID", id);
	}

	

}