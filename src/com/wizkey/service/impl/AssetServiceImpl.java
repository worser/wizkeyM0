package com.wizkey.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wizkey.dao.IAssetDao;
import com.wizkey.dao.IBaseDao;
import com.wizkey.pojo.Asset;
import com.wizkey.service.IAssetService;
import com.wizkey.util.PageModel;

@Service
public class AssetServiceImpl extends BaseServiceImpl<Asset> implements IAssetService{
	
	@Autowired
	protected IAssetDao assetDao;

	@Override
	protected IBaseDao<Asset> getDao() {
		// TODO Auto-generated method stub
		return assetDao;
	}

	/**
	 * 首页请求热门、最新方案 根据评论量或方案修改日期索引查询前10条方案
	 * 
	 * @param type
	 *            方案类型（热门或最新）
	 * @return Asset对象集合
	 */
	@Override
	public List<Asset> getAssetInIndex(String type) {
		// TODO Auto-generated method stub
//		List<Asset> list = assetDao.pageAll(1, 10, type).getList();
		return assetDao.pageAll(1, 10, type).getList();
	}

	/**
	 * 请求热门、最新方案列表 根据评论量或方案修改日期索引查询全部方案
	 * 
	 * @param type
	 *            方案类型（热门或最新）
	 * @param pageNo
	 *            当前页码
	 * @return PageModel对象
	 */
	@Override
	public PageModel<Asset> getMoreAsset(String type, String pageNo) {
		// TODO Auto-generated method stub
//		List<Asset> list = assetDao.pageAll(Integer.parseInt(pageNo), 20, type).getList();
		return assetDao.pageAll(Integer.parseInt(pageNo), 20, type);
	}

	/**
	 * 搜索方案列表 根据关键词匹配查询全部方案
	 * 
	 * @param keyword
	 *            关键词
	 * @param pageNo
	 *            当前页码
	 * @return PageModel对象
	 */
	@Override
	public PageModel<Asset> getAssetInList(String keyword, String pageNo) {
		// TODO Auto-generated method stub
//		List<Asset> list = assetDao.pageByProp(Integer.parseInt(pageNo), 20, "title", keyword).getList();
		return assetDao.pageByProp(Integer.parseInt(pageNo), 20, "title", keyword);
	}

	/**
	 * 搜索方案列表排序 根据关键词匹配查询全部方案并排序
	 * 
	 * @param type
	 *            排序类型
	 * @param keyword
	 *            关键词
	 * @param pageNo
	 *            当前页码
	 * @return PageModel对象
	 */
	@Override
	public PageModel<Asset> getAssetOrderedInList(String type, String keyword, String pageNo) {
		// TODO Auto-generated method stub
//		List<Asset> list = assetDao.pageByProp(Integer.parseInt(pageNo), 20, "title", keyword, type).getList();
		return assetDao.pageByProp(Integer.parseInt(pageNo), 20, "title", keyword, type);
	}

}
