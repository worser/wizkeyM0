package com.wizkey.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wizkey.dao.IBaseDao;
import com.wizkey.dao.ICommentDao;
import com.wizkey.pojo.Comment;
import com.wizkey.service.ICommentService;

@Service
public class CommentServiceImpl extends BaseServiceImpl<Comment> implements ICommentService {

	@Autowired
	protected ICommentDao commentDao;

	@Override
	protected IBaseDao<Comment> getDao() {
		return commentDao;
	}

	@Override
	public Object addComment(Comment comment, String userId, String assetId) {
		if (comment.getComment() != null) {
			boolean b = true;
			// 敏感违法信息过滤
			if (isLegal(comment.getComment())) {
				return "illegal";
			}
			if (comment.getScore() != 0) {
				b = commentDao.addScore(comment, userId, assetId);
			}
			return b&commentDao.addComment(comment, userId, assetId);
		}
		if (comment.getScore() != 0) {
			return commentDao.addScore(comment, userId, assetId);
		}
		return false;
	}

	public boolean isLegal(String comment) {
		// TODO 敏感违法信息过滤

		return false;
	}
}
