package com.wizkey.service;

import java.util.List;

import com.wizkey.pojo.Asset;
import com.wizkey.util.PageModel;

public interface IAssetService extends IBaseService<Asset>{

	/**
	 * 在首页根据指定类型（热点或最新）显示方案<br>
	 * 
	 * @param type
	 *            方案类型（热点或最新）
	 */
	List<Asset> getAssetInIndex(String type);
	
	/**
	 * 在列表页根据类型（热点或最新）分页显示更多方案<br>
	 * 
	 * @param type
	 *            方案类型（热点或最新）
	 * @param pageNo
	 *            列表页数
	 */
	PageModel<Asset> getMoreAsset(String type, String pageNo);
	
	/**
	 * 在列表页根据搜索关键词列表分页显示方案<br>
	 * 默认排序方式，后期在实现类中加入排序优化算法
	 * 
	 * @param keyword
	 *            搜索关键词
	 * @param pageNo
	 *            列表页数
	 */
	PageModel<Asset> getAssetInList(String keyword, String pageNo);
	
	/**
	 * 在列表页根据搜索关键词按指定排序方式分页显示更多方案<br>
	 * 
	 * @param type
	 *            方案排序类型
	 * @param keyword
	 *            搜索关键词
	 * @param pageNo
	 *            列表页数
	 */
	PageModel<Asset> getAssetOrderedInList(String type,String keyword, String pageNo);
}
