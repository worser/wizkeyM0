package com.wizkey.service;

import java.util.List;

import com.wizkey.pojo.Comment;
import com.wizkey.pojo.User;

public interface IUserService extends IBaseService<User>{

	/**
	 * 通过用户名和密码获得User<br>
	 */
	User getUserByPassword(String name,String password);
	/**
	 * 判断用户名是否存在<br>
	 */
	boolean existUserName(String name);
	
	User getUserByName(String username);
	
	List<Comment> getCommentsById(String id);

}
