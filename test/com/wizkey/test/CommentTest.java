package com.wizkey.test;

import java.sql.Timestamp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.wizkey.dao.IAssetDao;
import com.wizkey.pojo.Asset;
import com.wizkey.pojo.Comment;
import com.wizkey.pojo.User;
import com.wizkey.service.ICommentService;
import com.wizkey.service.IUserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-mgo.xml")
public class CommentTest {
	@Autowired
	private IUserService userService;
	@Autowired
	private ICommentService commentService;
	@Autowired
	private IAssetDao assetDao;

	@Test
	public void saveAsset(){
		Asset asset = new Asset();
		asset.setTitle("java文档");
		asset.setUrl("www.ibm/23");
		asset.setScore(0);
		assetDao.save(asset);
		
	}
	@Test
	public void addComment() {
		User user = userService.getUserByPassword("ws", "123");
		Asset asset = assetDao.findById("5a3cd1c9e746500be2c64760");
		Comment comment1 = new Comment(null, 1);
		commentService.addComment(comment1, user.getId(), asset.getId());
	}
}

