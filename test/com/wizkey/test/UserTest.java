package com.wizkey.test;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.wizkey.pojo.Asset;
import com.wizkey.pojo.User;
import com.wizkey.service.IAssetService;
import com.wizkey.service.IUserService;
import com.wizkey.util.PageModel;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-mgo.xml") 
public class UserTest {

    @Autowired
	private IUserService service;
    @Autowired
    private IAssetService assetService;
	@Test
	public void save() { 
		User user = new User();
		user.setName("ws");
		user.setAge(5);
		user.setBirth(new Timestamp(System.currentTimeMillis()));
		user.setPassword("123");
		service.save(user);
		System.out.println("已生成ID:" + user.getName());
	}
	
	@Test
	public void saveAsset(){
		Asset asset = new Asset("www.ibm/123", "wans", "1.1", new Date(), 0, 0);
		assetService.save(asset);
	}
	@Test
	public void getUserByPassword() {
		User user = service.getUserByPassword("ws", "123");
		System.out.println(user.toString());
	}

	@Test
	public void check(){
		User user = service.getUserByPassword("赵四", "123456");
		System.out.println(user.toString());
	}
	@Test
	public void update() {
		User user = service.getUserByPassword("ws", "123");
		user.setAge(90);
		user.setName("3sdfd");
		service.update(user);
	}
 
	@Test
	public void delete() {
		service.getUserByPassword("ws", "123");
	}

	@Test
	public void findAll() {
		List<User> list = service.findAll("birth desc");
		for (User u : list) {
			System.out.println(u.toString());
		}
	}

	@Test
	public void findByProp() {
		List<User> list = service.findByProp("name", "王爽","birth desc");
		for (User u : list) {
			System.out.println(u.getName());
		}
	}

	@Test
	public void findByProps() {
		List<User> list = service.findByProps(new String[] { "password", "age" }, new Object[] { "123", 13 });
		for (User u : list) {
			System.out.println(u.getName());
		}
	}

	@Test
	public void pageAll() {
		PageModel<User> page = service.pageAll(2, 1);
		System.out.println("总记录：" + page.getTotalCount() + "，总页数：" + page.getTotalPage());
		for (User u : page.getList()) {
			System.out.println(u.getName());
		}
	}

	@Test
	public void pageByProp() {
		PageModel<User> page = service.pageByProp(1, 1, "age", 12);
		System.out.println("总记录：" + page.getTotalCount() + "，总页数：" + page.getTotalPage());
		for (User u : page.getList()) {
			System.out.println(u.getName());
		}
	}

}
